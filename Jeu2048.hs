-- Types
data Direction = Droite | Haut | Gauche | Bas deriving (Eq)
type Tuile = Int
type Grille = [[Tuile]]

-- | Indique si un nombre entier désigne une tuile.
--
-- Une tuile est une puissance de 2 différente de 1.
--
-- >>> [estTuile i | i <- [0..8]]
-- [False,False,True,False,True,False,False,False,True]
estTuile :: Int -> Bool

-- | Indique si une grille est valide.
--
-- Une grille est valide si elle est de dimension 4 x 4 et que toutes les
-- valeurs qu'elle contient sont égales à 0 (qui identifie une case vide) ou à
-- une puissance de 2 (différente de 1).
--
-- >>> estValide [[2,4,2,2],[2,4,0,0],[0,0,0,2],[4,2,4,8]]
-- True
-- >>> estValide [[4,4,8,8],[8,2,3,4],[0,0,2,0],[4,2,4,8]]
-- False
--
-- Attention aux dimensions:
--
-- >>> estValide [[2,4,2,2],[0,0,2],[4,2,4,8]]
-- False
estValide :: Grille -> Bool

-- | Déplace les tuiles d'une ligne vers la gauche, en fusionnant les tuiles de
-- même valeur.
-- 
-- Si les cases à gauches sont inoccupées, alors les tuiles occupées glissent
-- le plus possible à gauche:
--
-- >>> deplacerLigne [0,0,2,0]
-- [2,0,0,0]
--
-- Si, après glissement, deux tuiles consécutives sont égales, on les fusionne:
--
-- >>> deplacerLigne [0,4,4,0]
-- [8,0,0,0]
-- >>> deplacerLigne [2,0,0,2]
-- [4,0,0,0]
--
-- Finalement, il est possible qu'il y ait deux fusions dans la même ligne,
-- mais une tuile ne peut jamais être impliquée plus d'une fois dans une
-- fusion:
--
-- >>> deplacerLigne [2,2,2,2]
-- [4,4,0,0]
-- >>> deplacerLigne [2,2,4,2]
-- [4,4,2,0]
deplacerLigne :: [Tuile] -> [Tuile]

-- | Déplace la grille complète dans une des quatre directions.
--
-- Cela revient à déplacer chaque ligne dans la même direction.
--
-- >>> deplacer Gauche [[2,2,4,4],[0,2,2,0],[0,2,0,2],[4,0,2,2]]
-- [[4,8,0,0],[4,0,0,0],[4,0,0,0],[4,4,0,0]]
-- >>> deplacer Haut [[2,2,4,4],[0,2,2,0],[0,2,0,2],[4,0,2,2]]
-- [[2,4,4,4],[4,2,4,4],[0,0,0,0],[0,0,0,0]]
-- >>> deplacer Droite [[2,2,4,4],[0,2,2,0],[0,2,0,2],[4,0,2,2]]
-- [[0,0,4,8],[0,0,0,4],[0,0,0,4],[0,0,4,4]]
-- >>> deplacer Bas [[2,2,4,4],[0,2,2,0],[0,2,0,2],[4,0,2,2]]
-- [[0,0,0,0],[0,0,0,0],[2,2,4,4],[4,4,4,4]]
deplacer :: Direction -> Grille -> Grille
